import { Grid } from "@mui/material";

export default function ENFJ() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Người chỉ dạy</Grid>
            <p>Dịch vụ cộng đồng và xã hội (Giáo dục sức khỏe, Tư vấn viên hôn nhân gia đình,...);</p>
            <p>Phương tiện – truyền thông (Biên tập viên, Quan hệ công chúng, Tác giả,...);</p>
            <p> Giáo dục (Giáo viên, Quản trị viên,...); </p>
            <p>Kinh doanh, quản lý và bán hàng (Marketing, Quản trị nhân sự, Quản lý kinh doanh,...); </p>
            <p> Giải trí, nghệ thuật và thiết kế;</p>
            <p> Dịch vụ chăm sóc cá nhân (Huấn luyện viên cá nhân, bảo mẫu,...); </p>
            <p>Khoa học (Nhà tâm lý học, nhà xã hội học,...); </p>
            <p>Pháp luật (Luật sư,...); </p>
            <p>Chăm sóc sức khỏe (Chuyên gia dinh dưỡng, Bác sỹ vật lý trị liệu,...);</p>
            <p>Văn phòng và hành chính.</p>
        </Grid>
    );
}

export function ENFP() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Người truyền cảm
hứng</Grid>
            <p>Nghệ thuật, thiết kế và giải trí (Diễn viên, nhạc sỹ, ca sỹ,...);</p>
            <p>
                Kinh doanh, quản lý và bán hàng (Marketing, Quản trị nhân sự, Quản lý kinh doanh,...);
            </p>
            <p>
                Dịch vụ chăm sóc cá nhân (Huấn luyện viên cá nhân, bảo mẫu, Thợ cắt tóc...);
            </p>
            <p>
                Phương tiện – truyền thông (Biên tập viên, Quan hệ công chúng, Tác giả,...);
            </p>
            <p>
                Khoa học (Nhà tâm lý học, nhà xã hội học,...);
            </p>
            <p>
                Giáo dục (Giáo viên, Quản trị viên,...);
            </p>
            <p>
                Chăm sóc sức khỏe (Chuyên gia dinh dưỡng, Bác sỹ vật lý trị liệu,...);
            </p>
            <p>
                Dịch vụ cộng đồng và xã hội (Giáo dục sức khỏe, Tư vấn viên hôn nhân gia đình,...)
            </p>
        </Grid>
    );
}

export function ENTJ() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Nhà điều hành</Grid>

            <p>Kinh doanh và tài chính (Quản lý kinh doanh, chuyên gia tài chính , giám đốc điều hành,...);</p>
            <p>
                Lãnh đạo và quản lý (Quản lý nhân sự, Chủ tịch hội đồng,...);
            </p>
            <p>
                Kiến trúc và kỹ thuật (Kỹ sư, Kiến trúc sư, Kiểm soát viên, Nhà thầu xây dựng...);
            </p>
            <p>
                Khoa học và đời sống (Nhà kinh tế, Nhà tâm lý, Nhà khoa học,...);
            </p>
            <p>
                Nghệ thuật và truyền thông (Quan hệ công chúng, Giám đốc sản xuất nghệ thuật,...);
            </p>
            <p>
                Chăm sóc sức khỏe (Bác sỹ, quản lý dịch vụ y tế,...);
            </p>
            <p>
                Giáo dục và đào tạo (Giảng viên, Giáo sư tiến sỹ,...);
            </p>
            <p>
                Giải trí và thể thao (Huấn luyện viên, Nhà sản xuất,..);
            </p>
            <p>
                Nhóm ngàng IT;
            </p>
            <p>
                Pháp luật (Thanh tra, Luật sư, Cảnh sát điều tra,...).
            </p>
        </Grid>
    );
}

export function ENTP() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Người có tầm nhìn xa</Grid>

            <p>Kinh doanh và lãnh đạo (Quản lý kinh doanh, quản trị nhân sự, giám đốc điều hành,...);</p>
            <p>
                Nghệ thuật và thiết kế (Kiến trúc sư, Nhà sản xuất nghệ thuật,....);
            </p>
            <p>
                Khoa học và kỹ thuật (Nhà khoa học, kỹ sư, giáo sư tiến sỹ,...).
            </p>
        </Grid>
    );
}

export function ESFJ() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Người quan tâm</Grid>

            <p>Dịch vụ cộng đồng và xã hội (Giáo dục sức khỏe, Tư vấn viên,...);</p>
            <p>
                Phương tiện – truyền thông (Biên tập viên, Quan hệ công chúng, Tác giả,...);
            </p>
            <p>
                Giáo dục (Giáo viên, Quản trị viên,...);
            </p>
            <p>
                Kinh doanh, quản lý và bán hàng (Marketing, Quản trị nhân sự, Quản lý kinh doanh,...);
            </p>
            <p>
                Giải trí, nghệ thuật và thiết kế;
            </p>
            <p>
                Dịch vụ chăm sóc cá nhân (Huấn luyện viên cá nhân, bảo mẫu,...);
            </p>
            <p>
                Khoa học (Nhà tâm lý học, nhà xã hội học,...);
            </p>
            <p>
                Pháp luật (Cảnh sát, tòa án...);
            </p>
            <p>
                Pháp luật (Cảnh sát, tòa án...);
                Chăm sóc sức khỏe (Chuyên gia dinh dưỡng, Bác sỹ, Y tá...);
            </p>
            <p>
                Văn phòng và hành chính.
            </p>
        </Grid>
    );
}

export function ESFP() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Người trình diễn</Grid>

            <p>Dịch vụ cộng đồng và xã hội (Giáo dục sức khỏe, Tư vấn viên,...); </p>
            <p>Phương tiện – truyền thông (Biên tập viên, Quan hệ công chúng, Tác giả,...); </p>
            <p>Giáo dục (Giáo viên, Quản trị viên,...);  </p>
            <p>Kinh doanh, quản lý và bán hàng (Marketing, Quản trị nhân sự, Quản lý kinh doanh,...);  </p>
            <p>Giải trí, nghệ thuật và thiết kế (Ca sỹ, Nhạc sỹ, Thiết kế thời trang,...);  </p>
            <p>Dịch vụ sức khỏe và chăm sóc cá nhân (Huấn luyện viên cá nhân, bảo mẫu, Y tá...);  </p>
            <p>Cảnh sát, lính cứu hỏa.  </p>
        </Grid>
    );
}

export function ESTJ() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Người giám sát</Grid>

            <p> Kinh doanh, bán hàng và tài chính (Cố vấn tài chính, Kế toán, Quản lý kinh doanh,...);</p>
            <p> Văn phòng và hành chính;</p>
            <p>  Quản lý;</p>
            <p>  Kiến trúc sư, kỹ thuật viên và kỹ sư kỹ thuật;</p>
            <p>  Khoa học và đời sống (Kỹ thuật viên nông lâm nghiệp, nhà sinh học môi trường,...);</p>
            <p>  Nông lâm nghiệp;</p>
            <p>  Kỹ thuật bảo trì và sửa chữa (Cơ khí ô tô, nhân viên điện lạnh, điện tử,...);</p>
            <p>  Vận chuyển;</p>
            <p>  Sản xuất;</p>
            <p>  Giải trí, thể thao;</p>
            <p>  Dịch vụ cộng đồng và xã hội (Giáo dục sức khỏe, Tư vấn viên, nhân viên xã hội...);</p>
            <p>  Chăm sóc sức khỏe (Chuyên gia dinh dưỡng, Bác sỹ, Y tá, Điều dưỡng...);</p>
            <p>  Giáo dục (Giáo viên, Quản trị viên, Thủ thư...);</p>
            <p>  Cảnh sát, vệ sỹ, lính cứu hỏa, quân đội;</p>
            <p> Pháp luật (Tòa án, luật sư...).</p>
        </Grid>
    );
}

export function ESTP() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Người thực thi</Grid>

            <p>Khoa học và đời sống (Kỹ thuật viên nông lâm nghiệp, nhà sinh học môi trường,...); </p>
            <p> Giáo dục (Giáo viên dạy nghề, Quản trị viên,...);   </p>
            <p> Kinh doanh, bán hàng và tài chính (Cố vấn tài chính, Quản trị nhân sự, Quản lý kinh
doanh,...); </p>
            <p>Kỹ thuật bảo trì và sửa chữa (Cơ khí ô tô, nhân viên điện lạnh, điện tử,...);  </p>
            <p>  Sản xuất (Thợ mộc, thợ làm bánh, thanh tra giám sát chất lượng);</p>
            <p>Kiến trúc sư và kỹ sư kỹ thuật;  </p>
            <p>Cảnh sát, vệ sỹ, lính cứu hỏa, sỹ quan quân đội, phi công;  </p>
            <p>  Xây dựng.</p>
        </Grid>
    );
}

export function INFJ() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Người che chở</Grid>

            <p>Dịch vụ cộng đồng và xã hội (Giáo dục sức khỏe, Tư vấn viên hôn nhân gia đình,...); </p>
            <p>Giáo dục (Giáo viên, Quản trị viên,...); </p>
            <p>Dịch vụ chăm sóc cá nhân (Huấn luyện viên cá nhân, bảo mẫu,...);  </p>
            <p> Khoa học (Nhà tâm lý học, nhà xã hội học,...); </p>
            <p> Pháp luật (Luật sư,...); </p>
            <p> Chăm sóc sức khỏe (Chuyên gia dinh dưỡng, Bác sỹ vật lý trị liệu,...); </p>
            <p>  Giải trí, nghệ thuật và thiết kế;</p>
            <p>  Nhân sự.</p>
        </Grid>
    );
}

export function INFP() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Người duy tâm</Grid>

            <p>Nghệ thuật và thiết kế (thiết kế thời trang, thiết kế mỹ thuật,...); </p>
            <p>Dịch vụ cộng đồng và xã hội (Giáo dục sức khỏe, quản lý dịch vụ cộng đồng,...); </p>
            <p>Giáo dục (Giáo viên, Quản trị viên, Thủ thư...);  </p>
            <p>Chăm sóc sức khỏe (Chuyên gia dinh dưỡng, Bác sỹ vật lý trị liệu,...);  </p>
            <p>Kinh doanh, quản lý và bán hàng (Marketing, Quản trị nhân sự, Quản lý kinh doanh,...);  </p>
            <p>Phương tiện – truyền thông (Biên tập viên, Quan hệ công chúng, Tác giả,...);  </p>
            <p>Khoa học (Nhà tâm lý học, nhà xã hội học,...).  </p>
        </Grid>
    );
}

export function INTJ() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Nhà khoa học</Grid>

            <p> Kinh doanh và tài chính (Kế toán, Chuyên gia phân tích tài chính , giám đốc điều hành,...);</p>
            <p> Toán học ( Nhà toán học, Nhà thống kê, phân tích nghiên cứu hoạt động,...);</p>
            <p>Kiến trúc và kỹ thuật (Kỹ sư, Kiến trúc sư, Kiểm soát viên, Nhà thầu xây dựng...);  </p>
            <p> Khoa học và đời sống (Nhà kinh tế, Nhà tâm lý, Nhà khoa học,...); </p>
            <p>Nghệ thuật và truyền thông (Quan hệ công chúng, Giám đốc sản xuất nghệ thuật,...);  </p>
            <p>Chăm sóc sức khỏe (Bác sỹ, quản lý dịch vụ y tế,...);  </p>
            <p> Giáo dục và đào tạo (Giảng viên, Giáo sư tiến sỹ,...); </p>
            <p> Giải trí và thể thao (Huấn luyện viên, Nhà sản xuất,..); </p>
            <p> Nhóm ngành IT; </p>
            <p> Pháp luật (Thẩm phán, Luật sư, Cảnh sát điều tra,...). </p>
        </Grid>
    );
}

export function INTP() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Nhà tư duy</Grid>

            <p>Nhóm ngành công nghệ IT (Phát triển phần mềm, nhà phát triển web,...); </p>
            <p> Kỹ sư kỹ thuật;</p>
            <p> Khoa học (Nhà khoa học, nhà tâm lý học, giáo sư tiến sỹ,...); </p>
            <p> Kinh doanh và tài chính (chuyên gia tài chính, kỹ sư bán hàng, Nghiên cứu thị trường,..); </p>
            <p> Giải trí và nghệ thuật (Nhiếp ảnh gia, Biên tập viên, Nhạc sỹ,...). </p>
        </Grid>
    );
}

export function ISFJ() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Người nuôi dưỡng</Grid>

            <p>Dịch vụ cộng đồng và xã hội (Giáo dục sức khỏe, Tư vấn viên, nhân viên xã hội...); </p>
            <p> Khoa học và đời sống (Kỹ thuật viên nông lâm nghiệp, nhà sinh học môi trường,...);</p>
            <p>Chăm sóc sức khỏe (Chuyên gia dinh dưỡng, Bác sỹ, Y tá, Điều dưỡng...);  </p>
            <p> Giáo dục (Giáo viên, Quản trị viên, Thủ thư...); </p>
            <p> Văn phòng và hành chính; </p>
            <p>Dịch vụ chăm sóc cá nhân (Huấn luyện viên cá nhân, bảo mẫu, Thợ làm móng...);  </p>
            <p> Kinh doanh, bán hàng và tài chính (Cố vấn tài chính, Quản trị nhân sự, Quản lý kinh
doanh,...); </p>
            <p> Kỹ thuật bảo trì và sửa chữa (Cơ khí ô tô, nhân viên điện lạnh, điện tử,...); </p>
            <p> Sản xuất (Thợ mộc, thợ làm bánh, thanh tra giám sát chất lượng); </p>
            <p> Kiến trúc sư và kỹ sư kỹ thuật; </p>
            <p> Vận chuyển; </p>
            <p> Cảnh sát, vệ sỹ, lính cứu hỏa; </p>
            <p> Pháp luật (Tòa án, luật sư...). </p>
        </Grid>
    );
}

export function ISFP() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Người nghệ sĩ</Grid>

            <p> Giáo dục (Giáo viên mầm non, Quản trị viên,...);</p>
            <p> Kinh doanh, quản lý và bán hàng (Marketing, Quản trị nhân sự, Quản lý kinh doanh,...);</p>
            <p> Giải trí, nghệ thuật và thiết kế (Ca sỹ, Nhạc sỹ, Thiết kế thời trang,...); </p>
            <p> Dịch vụ sức khỏe và chăm sóc cá nhân (Huấn luyện viên cá nhân, bảo mẫu, Y tá...); </p>
            <p> Cảnh sát, lính cứu hỏa; </p>
            <p>Kiến trúc sư, thợ mộc, thợ may, đầu bếp, thợ kim hoàn.  </p>
        </Grid>
    );
}


export function ISTJ() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Người trách nhiệm   </Grid>

            <p>Kinh doanh, bán hàng và tài chính (Cố vấn tài chính, Kế toán, Quản lý kinh doanh,...); </p>
            <p>Văn phòng và hành chính; </p>
            <p> Quản lý; </p>
            <p>Kiến trúc sư, kỹ thuật viên và kỹ sư kỹ thuật;  </p>
            <p> Khoa học và đời sống (Kỹ thuật viên nông lâm nghiệp, nhà sinh học môi trường,...); </p>
            <p>Nông lâm nghiệp;  </p>
            <p>Kỹ thuật bảo trì và sửa chữa (Cơ khí ô tô, nhân viên điện lạnh, điện tử,...);  </p>
            <p>Vận chuyển;  </p>
            <p> Sản xuất; </p>
            <p>Giải trí, thể thao;  </p>
            <p> Dịch vụ cộng đồng và xã hội (Giáo dục sức khỏe, Tư vấn viên, nhân viên xã hội...); </p>
            <p> Chăm sóc sức khỏe (Chuyên gia dinh dưỡng, Bác sỹ, Y tá, Điều dưỡng...); </p>
            <p> Giáo dục (Giáo viên, Quản trị viên, Thủ thư...); </p>
            <p>Cảnh sát, vệ sỹ, lính cứu hỏa, quân đội;  </p>
            <p> Pháp luật (Tòa án, luật sư...). </p>
        </Grid>
    );
}

export function ISTP() {
    return (
        <Grid sx={{ fontSize: '18px', lineHeight: '30px' }}>
            <Grid sx={{ fontSize: '24px', paddingTop: '15px', fontWeight: 900}}>Nhà cơ học</Grid>

            <p>Phân tích tài chính, chứng khoán, nhà kinh tế; </p>
            <p> Nhà sinh vật học, nhà địa chất;</p>
            <p> Dịch vụ sức khỏe và chăm sóc cá nhân (Huấn luyện viên cá nhân, bảo mẫu, Y tá...); </p>
            <p> Cảnh sát, lính cứu hỏa, sỹ quan quân đội, hình sự; </p>
            <p>Kiến trúc sư, thợ mộc, thợ may, đầu bếp, thợ kim hoàn;  </p>
            <p>Phi công, thuyền trưởng;  </p>
            <p>IT (Phát triển phần mềm, quản trị cơ sở dữ liệu,....).  </p>
        </Grid>
    );
}
