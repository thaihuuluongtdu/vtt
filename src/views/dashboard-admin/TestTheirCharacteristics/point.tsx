import {
  Button,
  Grid,
  TextField
} from "@mui/material";
import { useEffect, useState } from "react";
import { gridSpacing } from "store/constant";
import styled from "styled-components";
import MainCardV2 from "ui-component/cards/MainCardV2";
import validator from "validator";
import { useSubmit } from "./hook/submit";
// ==============================|| TABLE - STICKY HEADER ||============================== //

export default function PointTable(props: {
  [x: string]: any;
  statusSubmit?: any
}) {

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');
  const { hanldSubmit, isSubmit } = useSubmit(name, email, phone)

  useEffect(() => {
    props.statusSubmit(isSubmit)
  }, [isSubmit])

  const [messageEmail, setMessageEmail] = useState("");
  const [messagePhone, setMessagePhone] = useState("");

  const validateEmail = (e:string) => {
    setEmail(e)
    if (validator.isEmail(e)) {
      setMessageEmail("");
    } else {
      setMessageEmail("Phải nhập đúng email!");
    }
  };

  const validatePhone = (e:string) => {
    setPhone(e)
    if (e.length === 10 && e[0] === '0') {
      setMessagePhone("");
    } else {
      setMessagePhone("Phải nhập đúng SDT!");
    }
  };

  return (
    <>
      <MainCardV2 title="ĐỊNH HƯỚNG NGHỀ NGHIỆP" titleContent={`Bạn đang phân vân, loay hoay không biết mình thích nghề gì?`}
        titleContent1={`Mình phù hợp với ngành nghề nào?`}
        titleContent2={`Hãy bắt tay vào trả lời trắc nghiệm tính cách MBTI (Myers-Briggs Type Indicator), đây là phương
        pháp sử dụng các câu hỏi trắc nghiệm tâm lý, tính cách cũng như cách Bạn nhận thức thế giới
        xung quanh, đưa ra quyết định cho một vấn đề.`}>
        <Grid
          container
          spacing={gridSpacing}
          sx={{ flexWrap: "nowrap", justifyContent: "space-around", marginTop: '10px' }}
        >
          <CssTextField onChange={(e) => setName(e.target.value)} sx={{ width: '30%' }} id="outlined-basic" label="Họ và tên" variant="outlined" />
          <CssTextField onChange={(e) => validateEmail(e.target.value)} sx={{ width: '30%' }} type="email" id="outlined-basic" label="Email" variant="outlined" />
          <CssTextField onChange={(e) => validatePhone(e.target.value)} sx={{ width: '30%' }} type="number" id="outlined-basic" label="Số điện thoại" variant="outlined" />

        </Grid>

        <Grid sx={{ display: 'flex', justifyContent: 'right', alignItems: 'center', marginTop: '20px', gap: '20px' }}>
          {name === '' || email === '' || phone === '' ? <Grid>Bạn phải hoàn thành hết thông tin</Grid> : ''}
          <Grid>{messageEmail}</Grid>
          <Grid>{messagePhone}</Grid>
          <Button disabled={name === '' || email === '' || phone === '' || messageEmail !== '' || messagePhone !== ''} onClick={hanldSubmit} size='large' variant="outlined" sx={{ marginRight: '10px', width: '150px', height: '50px', gap: '10px', borderColor: '#680102', color: '#680102' }}>Tiếp tục</Button>
        </Grid>
      </MainCardV2>
    </>
  );
}

const CssTextField = styled(TextField)({
  '& label.Mui-focused': {
    color: '#680102',
  },
  '& .MuiInput-underline:after': {
    borderBottomColor: '#680102',
  },
  '& .MuiOutlinedInput-root': {

    '&:hover fieldset': {
      borderColor: '#680102',
    },
    '&.Mui-focused fieldset': {
      borderColor: '#680102',
    },
    '& input': { color: '#680102' }
  },
});