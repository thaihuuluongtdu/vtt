import {
  Button,
  Checkbox,
  FormControlLabel,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow
} from "@mui/material";
import React, { useEffect, useState } from "react";
import { gridSpacing } from "store/constant";
import IBreadcrumsCustom from "ui-component/breadcrums";
import MainCardV2 from "ui-component/cards/MainCardV2";
import ENFJ, { ENFP, ENTJ, ENTP, ESFJ, ESFP, ESTJ, ESTP, INFJ, INFP, INTP, ISFJ, ISFP, ISTJ, ISTP } from "./GrCharacter";
import { KeyedObject } from "types";
import axios from "axios";
import AlertStoryDelete from "views/application/kanban/Backlogs/AlertStoryDelete";

// ==============================|| TABLE - STICKY HEADER ||============================== //

export default function StickyHeadTable(props: {
  [x: string]: any;
  projectItem: any;

}) {

  let checkArray: any = []
  let anwerArray: any = []

  let anwerArrayA: any = []
  let anwerArrayB: any = []

  const storeAnswer = localStorage.getItem('arrayAnwerStore') || '[]'
  const storeChecked = localStorage.getItem('arrayCheckedStore') || '[]'

  const userName = localStorage.getItem('nameUser')
  const userEmail = localStorage.getItem('emailUser')
  const userPhone = localStorage.getItem('phoneUser')


  const [arrayAnwer, setArrayAnwer] = useState<any[]>(anwerArray);
  const [checkedArray, setcheckedArray] = useState<any[]>(checkArray);
  const [listAnwerArray, setListAnwerArrayB] = useState<any[]>(anwerArrayB);
  const [isNumber, setNumber] = useState('');
  const [isDone, setDone] = useState(true);
  const [isSubmit, setSubmit] = useState(true);
  const [openModal, setOpenModal] = useState(false);

  useEffect(() => {
    for (let i = 0; i < 76; i++) {
      anwerArrayA[i] = ''
      if (JSON.parse(storeAnswer)[i] !== '' && JSON.parse(storeChecked)[i] !== false) {
        anwerArray[i] = JSON.parse(storeAnswer)[i]
        checkArray[i] = JSON.parse(storeChecked)[i]
      }
    }
    setArrayAnwer(anwerArray)
    setcheckedArray(checkedArray)
    setListAnwerArrayB(anwerArrayA)
  }, [])


  const handleChange = (event: React.ChangeEvent<HTMLInputElement>, anwer: string, idAnwer: number) => {

    const newAnwerArray = arrayAnwer
    const newCheckArray = checkedArray

    if (event.target.checked) {
      newAnwerArray[idAnwer] = anwer
      newCheckArray[idAnwer] = true
    }
    if (!event.target.checked) {
      newAnwerArray[idAnwer] = ''
      newCheckArray[idAnwer] = false
    }
    setArrayAnwer(newAnwerArray)
    setcheckedArray(newCheckArray)
    localStorage.setItem('arrayAnwerStore', JSON.stringify(arrayAnwer));
    localStorage.setItem('arrayCheckedStore', JSON.stringify(checkedArray));
    setNumber(isNumber + 1)
  };

  useEffect(() => {
    setArrayAnwer(arrayAnwer)
    setcheckedArray(checkedArray)
  }, [isNumber])

  const col1 = [0, 7, 14, 21, 28, 35, 42, 49, 56, 63];
  const col2 = [1, 8, 15, 22, 29, 36, 43, 50, 57, 64];
  const col3 = [2, 9, 16, 23, 30, 37, 44, 51, 58, 65];
  const col4 = [3, 10, 17, 24, 31, 38, 45, 52, 59, 66];
  const col5 = [4, 11, 18, 25, 32, 39, 46, 53, 60, 67];
  const col6 = [5, 12, 19, 26, 33, 40, 47, 54, 61, 68];
  const col7 = [6, 13, 20, 27, 34, 41, 48, 55, 62, 69];

  let pointAnswerCol1: any = [];
  let pointAnswerCol2: any = [];
  let pointAnswerCol3: any = [];
  let pointAnswerCol4: any = [];
  let pointAnswerCol5: any = [];
  let pointAnswerCol6: any = [];
  let pointAnswerCol7: any = [];
  let countsPoint1: any = {};
  let countsPoint2: any = {};
  let countsPoint3: any = {};
  let countsPoint4: any = {};
  let countsPoint5: any = {};
  let countsPoint6: any = {};
  let countsPoint7: any = {};

  const [characterCol1, setCharacterCol1] = useState('');
  const [characterCol3, setCharacterCol3] = useState('');
  const [characterCol5, setCharacterCol5] = useState('');
  const [characterCol7, setCharacterCol7] = useState('');

  function submitData() {

    const newPoin = listAnwerArray

    for (let i = 0; i < 76; i++) {
      if (arrayAnwer[i] === props.projectItem[i].answer.A) {
        newPoin[i] = 'A'
      }
      if (arrayAnwer[i] === props.projectItem[i].answer.B) {
        newPoin[i] = 'B'
      }
    }
    setListAnwerArrayB(newPoin)

    for (let x = 0; x < col1.length; x++) {
      pointAnswerCol1[x] = listAnwerArray[col1[x]]
      pointAnswerCol2[x] = listAnwerArray[col2[x]]
      pointAnswerCol3[x] = listAnwerArray[col3[x]]
      pointAnswerCol4[x] = listAnwerArray[col4[x]]
      pointAnswerCol5[x] = listAnwerArray[col5[x]]
      pointAnswerCol6[x] = listAnwerArray[col6[x]]
      pointAnswerCol7[x] = listAnwerArray[col7[x]]

    }

    for (const num of pointAnswerCol1) {
      countsPoint1[num] = countsPoint1[num] ? countsPoint1[num] + 1 : 1;
    }
    for (const num of pointAnswerCol2) {
      countsPoint2[num] = countsPoint2[num] ? countsPoint2[num] + 1 : 1;
    }
    for (const num of pointAnswerCol3) {
      countsPoint3[num] = countsPoint3[num] ? countsPoint3[num] + 1 : 1;
    }
    for (const num of pointAnswerCol4) {
      countsPoint4[num] = countsPoint4[num] ? countsPoint4[num] + 1 : 1;
    }
    for (const num of pointAnswerCol5) {
      countsPoint5[num] = countsPoint5[num] ? countsPoint5[num] + 1 : 1;
    }
    for (const num of pointAnswerCol6) {
      countsPoint6[num] = countsPoint6[num] ? countsPoint6[num] + 1 : 1;
    }
    for (const num of pointAnswerCol7) {
      countsPoint7[num] = countsPoint7[num] ? countsPoint7[num] + 1 : 1;
    }

    if (countsPoint1['A'] > countsPoint1['B']) {
      setCharacterCol1('E')
    } else {
      setCharacterCol1('I')
    }

    if (countsPoint3['A'] + countsPoint2['A'] > countsPoint3['B'] + countsPoint2['B']) {
      setCharacterCol3('S')
    } else {
      setCharacterCol3('N')
    }

    if (countsPoint5['A'] + countsPoint4['A'] > countsPoint5['B'] + countsPoint4['B']) {
      setCharacterCol5('T')
    } else {
      setCharacterCol5('F')
    }

    if (countsPoint7['A'] + countsPoint6['A'] > countsPoint7['B'] + countsPoint6['B']) {
      setCharacterCol7('J')
    } else {
      setCharacterCol7('P')
    }
    setOpenModal(true)
   
  }

  useEffect(() => {
    for (let x = 0; x < arrayAnwer.length; x++) {
      if (arrayAnwer[x] !== undefined && arrayAnwer[x] !== null) {
        setSubmit(false)
      }
    }
    setDone(false)
  }, [arrayAnwer])

    const handleModalClose = (status: boolean) => {
        setOpenModal(false);

        axios.post('https://vtt-be-v2.onrender.com/api/checking/new', {
          name: userName,
          email: userEmail,
          phone: userPhone,
          arrayAnswer: arrayAnwer,
          timeAnswer: new Date(),
          resultCheck: characterCol1 + characterCol3 + characterCol5 + characterCol7
        })
          .then(function (response) {
            console.log('Thành công');
                localStorage.removeItem('arrayAnwerStore')
                localStorage.removeItem('arrayCheckedStore')
                localStorage.removeItem('nameUser')
                localStorage.removeItem('emailUser')
                localStorage.removeItem('phoneUser')
                setDone(true)
          })
          .catch(function (error) {
            console.log(error);
          });
    };

  return (
    <>
      <IBreadcrumsCustom profile="Trắc nghiệm tính cách" mainProfile="Trắc nghiệm tính cách" link="/tracnghiemtinhcach" />
      {/* {characterCol1 === '' && characterCol3 === '' && characterCol5 === '' && characterCol7 === '' ? */}
      {!isDone ?
        <>
          <MainCardV2 title={`Danh sách câu hỏi của - ${userName}`}>
            <Grid
              container
              spacing={gridSpacing}
              sx={{ flexWrap: "nowrap", justifyContent: "space-between" }}
            >
            </Grid>
            <TableContainer sx={{ maxHeight: 440 }}>
              <Table stickyHeader aria-label="sticky table">
                <TableHead>
                  <TableRow>
                    <TableCell >SỐ THỨ TỰ</TableCell>
                    <TableCell align="left">CÂU HỎI</TableCell>
                    <TableCell align="left">ĐÁP ÁN A</TableCell>
                    <TableCell align="left">ĐÁP ÁN B</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {props.projectItem
                    .map((row: KeyedObject, index: number) => (
                      <TableRow
                        sx={{ py: 3 }}
                        hover
                        role="checkbox"
                        tabIndex={-1}
                        key={row.code}
                      >
                        <TableCell component="th" scope="row">
                          {index + 1}
                        </TableCell>
                        <TableCell align="left">{row.question}</TableCell>
                        <TableCell align="left">
                          {/* <FormControlLabel control={<Checkbox defaultChecked={false} value={row.answer.A} onChange={handleChange} />} label={row.answer.A} /> */}
                          <FormControlLabel control={<Checkbox checked={arrayAnwer[index] === row.answer.A} disabled={arrayAnwer[index] === row.answer.B} value={row} onChange={(e) => handleChange(e, row.answer.A, index)} />} label={row.answer.A} />
                        </TableCell>
                        <TableCell align="left">
                          <FormControlLabel control={<Checkbox checked={arrayAnwer[index] === row.answer.B} disabled={arrayAnwer[index] === row.answer.A} value={row.answer.B} onChange={(e) => handleChange(e, row.answer.B, index)} />} label={row.answer.B} />
                        </TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </TableContainer>
            <Grid sx={{ display: 'flex', justifyContent: 'right', alignItems: 'center', marginTop: '20px', gap: '20px' }}>
              {!isSubmit && <Grid>Bạn phải hoàn thành hết câu hỏi</Grid>}
              <Button onClick={submitData}  size='large' variant="outlined" sx={{ marginRight: '10px', width: '150px', height: '50px', gap: '10px', borderColor: '#680102' }}>Hoàn Thành</Button>
            </Grid>
            {openModal && <AlertStoryDelete title="" open={openModal} handleClose={handleModalClose} />}
          </MainCardV2>
        </>
        :
        <>
          <MainCardV2 title={`NHÓM TÍNH CÁCH -- ${characterCol1}${characterCol3}${characterCol5}${characterCol7} -- NGÀNH NGHỀ PHÙ HỢP`}>
            <Grid
              container
              spacing={gridSpacing}
              sx={{ flexWrap: "nowrap", justifyContent: "left", paddingLeft: '30px' }}
            >
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'ENFJ' && <ENFJ />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'ENFP' && <ENFP />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'ENTJ' && <ENTJ />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'ENTP' && <ENTP />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'ESFJ' && <ESFJ />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'ESFP' && <ESFP />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'ESTJ' && <ESTJ />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'ESTP' && <ESTP />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'INFJ' && <INFJ />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'INFP' && <INFP />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'INTP' && <INTP />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'ISFJ' && <ISFJ />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'ISFP' && <ISFP />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'ISTJ' && <ISTJ />}
              {characterCol1 + characterCol3 + characterCol5 + characterCol7 === 'ISTP' && <ISTP />}
            </Grid>
            <Grid sx={{ display: 'flex', justifyContent: 'right', alignItems: 'center', marginTop: '20px' }}>
              <Button onClick={(e) => window.location.reload()} size='large' variant="outlined" sx={{ marginRight: '10px', width: '150px', height: '50px', gap: '10px' }}>Quay về</Button>
            </Grid>
          </MainCardV2>
        </>
      }
    </>
  );
}
